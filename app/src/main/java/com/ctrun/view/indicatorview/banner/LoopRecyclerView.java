package com.ctrun.view.indicatorview.banner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;

/**
 * Created by ctrun on 2017/12/13.
 */

public class LoopRecyclerView extends RecyclerView {
    private static final String TAG = LoopRecyclerView.class.getSimpleName();
    private static final int DEFAULT_LOOP_TIME = 3000;

    private long mLoopTime;
    private boolean mCanLoop = true;
    private boolean mLooping;

    private LoopTask mLoopTask;

    public LoopRecyclerView(Context context) {
        this(context, null);
    }

    public LoopRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoopRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init();
    }

    private void init() {
        new PagerSnapHelper().attachToRecyclerView(this);

        mLoopTask = new LoopTask(this);
    }

    public boolean isLooping() {
        return mLooping;
    }

    /***
     * 开始轮循
     * @param loopTime 轮循时间
     */
    public void startLoop(long loopTime) {
        //如果是正在轮循的话先停掉
        if(mLooping) {
            stopLoop();
        }

        mCanLoop = true;
        mLoopTime = loopTime;
        mLooping = true;
        postDelayed(mLoopTask, mLoopTime);
    }

    public void startLoop() {
        startLoop(DEFAULT_LOOP_TIME);
    }

    public void stopLoop() {
        mLooping = false;
        removeCallbacks(mLoopTask);
    }

    //触碰控件的时候，轮循应该停止，离开的时候如果之前是开启轮循的话则重新启动轮循
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        int action = ev.getAction();
        if (action == MotionEvent.ACTION_UP||action == MotionEvent.ACTION_CANCEL||action == MotionEvent.ACTION_OUTSIDE) {
            // 开始轮循
            if(mCanLoop) {
                startLoop(mLoopTime);
            }
        } else if (action == MotionEvent.ACTION_DOWN) {
            // 停止轮循
            if(mCanLoop) {
                stopLoop();
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public LoopAdapter getAdapter() {
        return (LoopAdapter) super.getAdapter();
    }

    @Override
    public void setAdapter(Adapter adapter) {
        if (!(adapter instanceof LoopAdapter)) {
            throw new IllegalArgumentException("adapter must  instanceof LoopAdapter!");
        }
        super.setAdapter(adapter);

        scrollToPosition(getAdapter().getRealItemCount()  * 100000);//开始时的偏移量
    }

    static class LoopTask implements Runnable {

        private final WeakReference<LoopRecyclerView> reference;

        LoopTask(LoopRecyclerView loopRecyclerView) {
            this.reference = new WeakReference<>(loopRecyclerView);
        }

        @Override
        public void run() {
            LoopRecyclerView recyclerView = reference.get();

            if(recyclerView != null){
                if (recyclerView.mLooping) {
                    recyclerView.smoothScrollBy(recyclerView.getWidth(), recyclerView.getWidth());

                    recyclerView.postDelayed(recyclerView.mLoopTask, recyclerView.mLoopTime);
                }
            }
        }
    }

}
