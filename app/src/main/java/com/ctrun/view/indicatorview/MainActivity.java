package com.ctrun.view.indicatorview;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.ctrun.view.indicatorview.banner.LoopAdapter;
import com.ctrun.view.indicatorview.bean.AdvertConfigBean;
import com.ctrun.view.indicatorview.databinding.ActivityMainBinding;
import com.ctrun.view.indicatorview.util.JsonFileUtils;
import com.google.gson.Gson;

@SuppressWarnings("FieldCanBeLocal")
public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding mBinding;
    private MyLoopAdapter mLoopAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mBinding.getRoot());
        setSupportActionBar(mBinding.toolbarActionbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        String jsonDataAdConfig = JsonFileUtils.readJsonFromAsset(this, "cateye_cinema_top_ad_list.json");
        AdvertConfigBean configBean = new Gson().fromJson(jsonDataAdConfig, AdvertConfigBean.class);
        mLoopAdapter = new MyLoopAdapter();

        //noinspection unchecked
        mBinding.vpBanner.bindLifecycle(getLifecycle()).setAutoLoop(true).setCanLoop(true).setAdapter(mLoopAdapter).create(configBean.config);
        mBinding.indicatorView.attachToViewPager2(mBinding.vpBanner.getViewPager2());
    }

    private class MyLoopAdapter extends LoopAdapter<AdvertConfigBean.AdvertDataBean, RecyclerView.ViewHolder> {
        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
            AdvertConfigBean.AdvertDataBean bean = getItemData(position);
            Glide.with(MainActivity.this)
                    .load(bean.image)
                    .apply(RequestOptions.centerCropTransform().dontAnimate())
                    .into((ImageView) holder.itemView);
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            ImageView imageView = new ImageView(parent.getContext());
            imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setOnClickListener(v -> {

            });

            return new RecyclerView.ViewHolder(imageView) {};
        }
    }

}